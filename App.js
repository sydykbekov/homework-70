import React from 'react';

import {createStore} from 'redux';
import {Provider} from 'react-redux';

import reducer from './src/store/reducer';
import Calculator from "./src/containers/Calculator/Calculator";

const store = createStore(reducer);

const App = props => (
    <Provider store={store}>
        <Calculator />
    </Provider>
);

export default App;