import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";

class Calculator extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.formula}>{this.props.formula}</Text>
                <Text style={styles.formula}>{this.props.result}</Text>
                <View style={styles.buttons}>
                    {this.props.values.map(value => <TouchableOpacity style={styles.button}
                                                                      onPress={() => this.props.add(value)}
                                                                      key={value}><Text
                        style={styles.values}>{value}</Text></TouchableOpacity>)}
                    {this.props.symbols.map(symbol => <TouchableOpacity style={styles.button}
                                                                        onPress={() => this.props.add(symbol)}
                                                                        key={symbol}><Text
                        style={styles.values}>{symbol}</Text></TouchableOpacity>)}
                    <TouchableOpacity style={styles.button} onPress={this.props.remove}><Text
                        style={styles.values}>{'<'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.props.clear}><Text
                        style={styles.values}>C</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.props.evaluate}><Text
                        style={styles.values}>=</Text></TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 50
    },
    buttons: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 25
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10,
        borderWidth: 1,
        borderColor: '#fff',
        width: '25%',
        height: 80,
    },
    values: {
        textAlign: 'center',
        lineHeight: 55,
        fontSize: 30
    },
    formula: {
        height: 60,
        fontSize: 50,
        textAlign: 'right'
    }
});

const mapStateToProps = state => {
    return {
        formula: state.formula,
        result: state.result,
        values: state.values,
        symbols: state.symbols
    }
};

const mapDispatchToProps = dispatch => {
    return {
        add: value => dispatch({type: 'ADD', value: value}),
        remove: () => dispatch({type: 'REMOVE'}),
        evaluate: () => dispatch({type: 'EVAL'}),
        clear: () => dispatch({type: 'CLEAR'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);
