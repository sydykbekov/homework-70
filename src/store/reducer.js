const initialState = {
    formula: '',
    result: '',
    values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    symbols: ['+', '-', '*', '/', '.', '(', ')']
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD':
            if (state.result !== '') {
                state.formula = '';
                state.result = '';
            }
            if (state.symbols.indexOf(state.formula[state.formula.length - 1]) >= 0 && isNaN(action.value)) {
                return {
                    ...state,
                    formula: state.formula.substring(0, state.formula.length - 1) + action.value
                };
            } else {
                return {
                    ...state,
                    formula: state.formula + action.value
                };
            }
        case 'REMOVE':
            return {
                ...state,
                formula: state.formula.substring(0, state.formula.length - 1)
            };
        case 'EVAL':
            try {
                return {
                    ...state,
                    result: eval(state.formula)
                }
            } catch (error) {
                 alert("Something went wrong. Please check your formula");
            }
            return state;
        case 'CLEAR':
            return {
                ...state,
                formula: '',
                result: ''
            };
        default:
            return state;
    }
};

export default reducer;